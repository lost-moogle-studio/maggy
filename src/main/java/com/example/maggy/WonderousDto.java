package com.example.maggy;

import java.util.List;

public class WonderousDto {
    private List<ItemDto> commonItems;
    private List<ItemDto> uncommonItems;
    private List<ItemDto> rareItems;
    private List<ItemDto> veryRareItems;
    private List<ItemDto> legendaryItems;
    private List<ItemDto> artifactItems;
    private List<ItemDto> UniqueItems;
    private List<ItemDto> unknownItems;

    public WonderousDto() {}

    public List<ItemDto> getCommonItems() {
        return commonItems;
    }

    public void setCommonItems(List<ItemDto> commonItems) {
        this.commonItems = commonItems;
    }

    public List<ItemDto> getUncommonItems() {
        return uncommonItems;
    }

    public void setUncommonItems(List<ItemDto> uncommonItems) {
        this.uncommonItems = uncommonItems;
    }

    public List<ItemDto> getRareItems() {
        return rareItems;
    }

    public void setRareItems(List<ItemDto> rareItems) {
        this.rareItems = rareItems;
    }

    public List<ItemDto> getVeryRareItems() {
        return veryRareItems;
    }

    public void setVeryRareItems(List<ItemDto> veryRareItems) {
        this.veryRareItems = veryRareItems;
    }

    public List<ItemDto> getLegendaryItems() {
        return legendaryItems;
    }

    public void setLegendaryItems(List<ItemDto> legendaryItems) {
        this.legendaryItems = legendaryItems;
    }

    public List<ItemDto> getArtifactItems() {
        return artifactItems;
    }

    public void setArtifactItems(List<ItemDto> artifactItems) {
        this.artifactItems = artifactItems;
    }

    public List<ItemDto> getUniqueItems() {
        return UniqueItems;
    }

    public void setUniqueItems(List<ItemDto> uniqueItems) {
        UniqueItems = uniqueItems;
    }

    public List<ItemDto> getUnknownItems() {
        return unknownItems;
    }

    public void setUnknownItems(List<ItemDto> unknownItems) {
        this.unknownItems = unknownItems;
    }
}
