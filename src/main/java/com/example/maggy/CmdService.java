package com.example.maggy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CmdService implements CommandLineRunner {

    private final static Logger logger = LoggerFactory.getLogger(CmdService.class);

    @Override
    public void run(String... args) throws Exception {

        List<String> header = new ArrayList<>();
        List<ItemDto> dtoList = new ArrayList<>();

        WonderousDto wonderousDto = new WonderousDto();

        List<HtmlTable> tableList = this.scrape().getByXPath("//table");

        for (int i = 1; i < tableList.size(); i++) {
            HtmlTable table = tableList.get(i);

            for (int r = 1; r < table.getRows().size(); r++) {
                HtmlTableRow row = table.getRow(r);

                ItemDto dto = new ItemDto();

                dto.setName(row.getCell(0).asNormalizedText());
                dto.setType(row.getCell(1).asNormalizedText());
                dto.setAttuned(row.getCell(2).asNormalizedText());
                dto.setSource(row.getCell(3).asNormalizedText());

                dtoList.add(dto);
//                if (r == 0) {
//                    // Header
//                    HtmlTableRow row = table.getRow(r);
//
//                    header = row.getCells().stream()
//                            .map(DomNode::asNormalizedText)
//                            .toList();
//                } else {
//                }
            }
            switch (i) {
                case 1:
                    wonderousDto.setCommonItems(dtoList);
                    break;
                case 2:
                    wonderousDto.setUncommonItems(dtoList);
                    break;
                case 3:
                    wonderousDto.setRareItems(dtoList);
                    break;
                case 4:
                    wonderousDto.setVeryRareItems(dtoList);
                    break;
                case 5:
                    wonderousDto.setLegendaryItems(dtoList);
                    break;
                case 6:
                    wonderousDto.setArtifactItems(dtoList);
                    break;
                case 7:
                    wonderousDto.setUniqueItems(dtoList);
                    break;
                case 8:
                    wonderousDto.setUnknownItems(dtoList);
                    break;
                default:
                    logger.info("beppinlol");
            }

            dtoList = new ArrayList<>();
            logger.info("beppin");
        }

        ObjectMapper mapper = new ObjectMapper();
        String data = mapper.writeValueAsString(wonderousDto);
        try (FileWriter writer = new FileWriter("wonderous_items.json")) {
            writer.write(data);
        } catch (IOException e) {
            logger.error("Failed to write JSON data to file", e);
        }
    }

    private HtmlPage scrape() {
        try (WebClient client = new WebClient()) {
            client.getOptions().setCssEnabled(false);
            client.getOptions().setJavaScriptEnabled(false);

            return client.getPage("http://dnd5e.wikidot.com/wondrous-items");
        } catch (IOException e) {
            logger.error("aids", e);
            throw new RuntimeException(e);
        }
    }
}
