package com.example.maggy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaggyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaggyApplication.class, args);
	}

}
