package com.example.maggy;

public class ItemDto {
    private String name;
    private String type;
    private String attuned;
    private String source;

    public ItemDto() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAttuned() {
        return attuned;
    }

    public void setAttuned(String attuned) {
        this.attuned = attuned;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
